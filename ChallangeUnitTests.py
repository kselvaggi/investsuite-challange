""" Unit tests for InvestSuite Challange

Several tests are run in order to check the code correctness, it can be easly extended
with new test scenarios.

Date: 		28 November 2020
Author: 	Kevin Selvaggi
"""
import unittest
from ChallangeFunction import countIntegerDigits

class ChallangeTestFunction(unittest.TestCase):

	############### INTEGER TESTS ###################
	def test_negative_integer(self):
		self.assertEqual(countIntegerDigits(-32), 2)
	
	def test_one_digit_integer(self):
		self.assertEqual(countIntegerDigits(2), 1)

	def test_two_digit_integer(self):
		self.assertEqual(countIntegerDigits(55), 2)

	def test_three_digit_integer(self):
		self.assertEqual(countIntegerDigits(256), 3)

	def test_four_digit_integer(self):
		self.assertEqual(countIntegerDigits(1655), 4)

	def test_five_digit_integer(self):
		self.assertEqual(countIntegerDigits(21323), 5)

	def test_six_digit_integer(self):
		self.assertEqual(countIntegerDigits(132456), 6)
		
		
	################# FLOATS TEST #####################
	def test_negative_float(self):
		self.assertEqual(countIntegerDigits(-32.33), 2)
		
	def test_one_digit_float(self):
		self.assertEqual(countIntegerDigits(2.23), 1)
	
	def test_two_digit_float(self):
		self.assertEqual(countIntegerDigits(55.55), 2)
	
	def test_three_digit_float(self):
		self.assertEqual(countIntegerDigits(256.634), 3)

	def test_four_digit_float(self):
		self.assertEqual(countIntegerDigits(1655.234), 4)

	def test_five_digit_float(self):
		self.assertEqual(countIntegerDigits(21323.12222224), 5)

	def test_six_digit_float(self):
		self.assertEqual(countIntegerDigits(132456.23), 6)
	
	def test_rounded_float(self):
		# The function shouldn't round up the input number
		self.assertEqual(countIntegerDigits(99.9999), 2)
		
	################# INPUT TEST #####################
	def test_NaN_input(self):
		# check that countIntegerDigits fails when the input is a string
		s = 'String Input'
		with self.assertRaises(ValueError):
			countIntegerDigits(s)
	
	def test_list_with_number_input(self):
		# check that countIntegerDigits fails when the input is a list
		l = [1]
		with self.assertRaises(TypeError):
			countIntegerDigits(l)

if __name__ == '__main__':
 unittest.main()