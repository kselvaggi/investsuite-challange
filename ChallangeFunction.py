""" Code for InvestSuite Challange 

The purpose is to write a correct and maintanable code for a Python function that accepts "a number" and outputs the number 
of (base-10) digits needed to write the integer part of that number on a piece of paper. Unit tests are provided on a separate 
file.

Date: 		28 November 2020
Author: 	Kevin Selvaggi
"""

import math

def numLen(number):
	"""  Python function that accepts "a number" and outputs the number of (base-10) digits needed to write the integer part of that number on a piece of paper.

    Parameters
    ----------
    number : a number
        The input number

    Returns
    -------
    int
        The number of digits
    """

	return len(str(abs(number)))

def countIntegerDigits(inputNumber):
	"""  Python function that accepts "a number" and outputs the number of (base-10) digits needed to write the integer part of that number on a piece of paper.

    Parameters
    ----------
    inputNumber : a number
        The input number

    Returns
    -------
    int
        The number of (base-10) digits needed to write the integer part
		
	Raises
	------
	ValueError
		If the input is not a number
	TypeError
		If the input is not a number type
    """
	# Local Variables
	integerPart: int = 0
	
	# Output Variable
	result: int = 0
	
	try:
			# Cast to int to obtain the integer part, if it is not a bumber throws ValueError exception
			integerPart = int(inputNumber)
			
			# Count decimals
			result = numLen(integerPart)
			
	except ValueError:
		raise # The input value is not a number
	except TypeError:
		raise # The input value is not a number type
	
	return result
